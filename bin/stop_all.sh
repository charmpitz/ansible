#!/bin/bash

for container in $(sudo lxc-ls); do 
	echo "Stop $container"; 
	sudo lxc-stop --name $container; 
done;