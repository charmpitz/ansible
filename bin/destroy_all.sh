#!/bin/bash

for container in $(sudo lxc-ls); do 
	echo "Destroy $container"; 
	sudo lxc-destroy --name $container; 
done;